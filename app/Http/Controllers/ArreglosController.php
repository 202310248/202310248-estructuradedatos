<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArreglosController extends Controller
{
    public function Arreglos(){
        // Creacion de un arreglo
        $array = [1, 2, 3, 4, 5];
        //print_r($array);
        echo "<br>";

        //Eliminar elemento de un arreglo
        unset($array[3]);
        //print_r($array);
        echo "<br>";

        //Agregar elemento a un arreglo
        $array[] = 6;
        //print_r($array);
        echo "<br>";

        //Editar elemento a un arreglo
        $array[2] = "Pepe";
        //print_r($array);

        //Recorrer e imprimir un arreglo
        foreach ($array as $i => $value) {
   	  echo $array[$i]."<br>";
	}
    }
}

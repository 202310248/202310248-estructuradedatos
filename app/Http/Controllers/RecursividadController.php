<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecursividadController extends Controller
{
    public function recursividad($Nfin, $N){
        $N += 1;
        if ($N<=$Nfin){
            echo $N;
            $this->recursividad($Nfin, $N);
            return $N;       
        }
        return $N;
    }
    public function Incrementable(){
      $variable = $this->recursividad(10, 1); 
      return view('Recursividad',['variable'=>$variable]);
    }
}
?>
<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;


class ListaFifoLifoController extends Controller
{
    public function listaFifoLifo(){
        $lista = new SplDoublyLinkedList;
        $lista->setIteratorMode(SplDoublyLinkedList::IT_MODE_LIFO);

        $lista->push(1);
        $lista->push(2);
        $lista->push(3);
        $lista->push(4);
        $lista->push(5);

        //print_r($lista);
        echo"<h2>Lista LIFO(PILA)</h2>";
        $lista-> rewind();
        while ($lista->valid()){
            echo $lista->current()."<br>";
            $lista->next();
        }


        $lista->setIteratorMode(SplDoublyLinkedList::IT_MODE_FIFO);
        echo"<h2>Lista FIFO(COLA)</h2>";
        $lista-> rewind();
        while ($lista->valid()){
            echo $lista->current()."<br>";
            $lista->next();
        }

    }
}

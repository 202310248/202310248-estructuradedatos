<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuickshortController extends Controller
{
    public function quick_sort($arr){

        if(count($arr) <= 1){
            return $arr;
        }
        else {
            $pivote = $arr[0];
            $izquierda = [];
            $derecha = [];
        }
        for($i = 1; $i < count($arr); $i++)
        {
            if($arr[$i] < $pivote){
                $izquierda[] = $arr[$i];
            }
            else{
                $derecha[] = $arr[$i];
            }
        }
        echo "<br><br>";
        echo implode(",",$izquierda)."Izquierda<br>";
        print_r($pivote);
        echo "Pivote <br>";
        echo implode(",",$derecha)." Derecha<br>";

        return array_merge($this->quick_sort($izquierda), array($pivote), $this->quick_sort($derecha));
    }

    public function OrdenamientoQuickShort(){
        $arreglo = [5,3,8,6,2,7];
        echo implode(",",$arreglo)." Antes<br>";
        $ordenamiento = $this->quick_sort($arreglo);
        echo implode(",",$ordenamiento)." Despues <br>";
    }

}


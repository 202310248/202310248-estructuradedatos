<!DOCTYPE html>
<html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>RESTAURANTE</title>
     <h1><mark><em><a href="#" class="text-danger">MAZU</a></em></mark></h1>
     <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

 </head>
 <body>
     <div class="container-fluid">
          <div class="row">
              <div class="col-12">
                  <ul class="nav nav-pills">
                         <li class="nav-item">
                              <select class="form-select" aria-label="Default select example">
                                    <option value="1">Registrarse</option>
                                    <option value="2">Crear cuenta</option>
                              </select>
                         </li>
                  </ul>
              </div>
          </div>
     </div>

     <div class="container">
          <div class="row">
               <div class="col-12">
                    <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label"><strong>Nombre</strong></label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name and last name">
               </div>

               <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label"><strong>Correo electronico</strong></label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
               </div>

               <div class="col-12">
                    <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label"><strong>Dirección</strong></label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Address">
               </div>
          </div>
     </div>

               <div class="container-fluid">
                   <div class="row">
                         <div class="col-12">
              
                          <select class="form-select" aria-label="Default select example">
                               <option selected>Menú</selected>
                               <option value="1">Platillos</option>
                               <option value="2">Bebidas</option>
                               <option value="3">Postres</option>
                          </select>

            
              
                  
                          <button type="button" class="btn btn-success">Realizar Pedido</button>      
    
                          <button type="button" class="btn btn-danger">Cancelar pedido</button>      
                  
            

                            <div class="mb-2">
                               <label for="exampleFormControlTextarea1"  class="form-label"><strong>Sugerencias</strong></label>
                               <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" ></textarea>
                            </div>
                         </div>
                    </div>
               </div>

 </body>
</html>